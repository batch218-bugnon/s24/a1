/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/* [Exponent Operator and Template Literals]
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

//insert code here...

let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}.`)

/* [Array Destructuring and Template Literals]
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

//insert code here...

let address = [258, "Washington Ave NW", "California", "90011"];

let [houseNumber, streetName, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${streetName}, ${state} ${zipCode}`);

/* [Object Destructuring and Template Literals]
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

let animal = {
	animalType: "crocodile",
	animalWeight: 1075,
	animalMeasurement: "20 ft 3 in"
}

let {animalType, animalWeight, animalMeasurement} = animal;

console.log(`Lolong was a salted ${animalType}. He weighed at ${animalWeight} kgs with a measurement of ${animalMeasurement}.`);


/* [forEach() method and Implicit Return(arrow function => )]
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

let num = [1, 2, 3, 4, 5];

num.forEach((num) => {
	console.log (`${num}`);
});

/* [reduce() method and Implicit Return(arrow function => )]
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers from array of numbers of instuction #9.
*/


let reducedArray = num.reduce((sum, a) => {
	return sum + a;
});
console.log(reducedArray);

/*  [Class-Based Object Blueprint / JavaScript Classes]
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new dog();

myDog.name =("Franki");
myDog.age =(5);
myDog.breed = ("Miniature Dachshund");
console.log(myDog);


// 14. Create a git repository named S24.